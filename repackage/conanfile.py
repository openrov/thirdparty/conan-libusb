from conans import ConanFile
import os

class RepackageConan(ConanFile):
    name = "RepackageConan"
    version = os.environ['CONAN_PACKAGE_VERSION']
    build_requires = "libusb/{}@openrov/stable".format( version )
    keep_imports = True

    def imports(self):
        package_dst = "package/opt/openrov"
        self.copy( "lib/*", excludes=( "*.la", "*.a" ), dst=package_dst )
        self.copy( "licenses/*", dst=package_dst )